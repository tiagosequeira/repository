from turtle import Turtle

FONT = ("Courier", 24, "normal")


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.level = 1
        self.create_scoreboard()

    def create_scoreboard(self):
        self.penup()
        self.goto(x=-200, y=250)
        self.hideturtle()
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.write(f"Level: {self.level}", align="center", font=FONT)
        self.increase_level()

    def increase_level(self):
        self.level += 1

    def game_over(self):
        self.home()
        self.write("GAME OVER", align="center", font=FONT)

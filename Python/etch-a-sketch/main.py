from turtle import Turtle, Screen

tim = Turtle()
screen = Screen()


def move_backwards():
    tim.backward(10)


def move_forwards():
    tim.forward(10)


def rotate_left():
    new_heading = tim.heading() + 10
    tim.setheading(new_heading)


def rotate_right():
    new_heading = tim.heading() - 10
    tim.setheading(new_heading)


def clear():
    tim.clear()
    tim.penup()
    tim.home()
    tim.pendown()


screen.listen()

screen.onkey(move_forwards, "w")
screen.onkey(move_backwards, "s")
screen.onkey(rotate_right, "d")
screen.onkey(rotate_left, "a")
screen.onkey(clear, "c")
# screen.onkey(key="space", fun=move_backwards)
screen.exitonclick()

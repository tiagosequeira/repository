from turtle import Turtle, Screen
import random

is_race_on = False
screen = Screen()
screen.setup(width=500, height=400)
user_bet = screen.textinput(title="Make a bet", prompt="Which turtle?: ")

colors = ["red", "orange", "yellow", "green", "blue", "purple"]
pos_y = [-150, -100, -50, 0, 50, 100]

all_turtles = []

for i in range(0, 6):
    t1 = Turtle(shape="turtle")
    t1.color(colors[i])
    t1.penup()
    t1.goto(x=-230, y=pos_y[i])
    all_turtles.append(t1)

if user_bet:
    is_race_on = True

winning_color = ""
while is_race_on:
    for turtle in all_turtles:
        if turtle.xcor() > 230:
            winning_color = turtle.pencolor()
            is_race_on = False
        random_distance = random.randint(0, 10)
        turtle.forward(random_distance)

if winning_color == user_bet:
    print("you win!! ")
else:
    print("you lose")

print(f"{winning_color} won")


screen.exitonclick()

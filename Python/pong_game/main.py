import time
from turtle import Screen

from ball import Ball
from paddle import Paddle
from scoreboard import Scoreboard

INITIAL_POSITION = [(350, 0), (-350, 0)]

WIDTH = 800
HEIGHT = 600

screen = Screen()
screen.bgcolor("black")
screen.title("Pong Game")
screen.setup(width=WIDTH, height=HEIGHT)
screen.tracer(0)

r_paddle = Paddle(INITIAL_POSITION[0])
l_paddle = Paddle(INITIAL_POSITION[1])
ball = Ball()
scoreboard = Scoreboard()

screen.listen()

screen.onkey(r_paddle.move_up, "Up")
screen.onkey(r_paddle.move_down, "Down")

screen.onkey(l_paddle.move_up, "w")
screen.onkey(l_paddle.move_down, "s")

game_is_on = True
while game_is_on:
    time.sleep(ball.ball_speed)
    screen.update()
    ball.move()

    if ball.ycor() > 280 or ball.ycor() < -280:
        ball.bounce_y()

    if ((ball.distance(r_paddle) < 50 and ball.xcor() > 320)
            or (ball.distance(l_paddle) < 50 and ball.xcor() < -320)):
        ball.bounce_x()
        # ball.bounce_y()
    if ball.xcor() > 380:
        scoreboard.l_paddle_score()
        ball.reset_position()

    if ball.xcor() < -380:
        scoreboard.r_paddle_score()
        ball.reset_position()
screen.exitonclick()

from turtle import Turtle


class Ball(Turtle):
    def __init__(self):
        super().__init__()
        self.ball_speed = 0.1
        self.x_move = 10
        self.y_move = 10
        self.create_ball()
        self.move()

    def create_ball(self):
        self.color("white")
        self.shape("circle")
        self.penup()
        # self.goto(position)

    def move(self):
        new_y = self.ycor() + self.y_move
        new_x = self.xcor() + self.x_move
        self.goto(x=new_x, y=new_y)

    def bounce_y(self):
        self.y_move *= -1

    def bounce_x(self):
        self.ball_speed *= 0.9
        self.x_move *= -1

    def reset_position(self):
        self.home()
        self.ball_speed = 0.1
        self.bounce_x()

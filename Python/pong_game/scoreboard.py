from turtle import Turtle

SCORE_POSITION = [(100, 200), (-100, 200)]


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.r_score = 0
        self.l_score = 0
        self.color("white")
        self.penup()
        self.hideturtle()
        self.update_scoreboard()

    def update_scoreboard(self):
        self.clear()
        self.goto(SCORE_POSITION[1])
        self.write(self.l_score, align="center", font=("Courier", 80, "normal"))
        self.goto(SCORE_POSITION[0])
        self.write(self.r_score, align="center", font=("Courier", 80, "normal"))

    def l_paddle_score(self):
        self.l_score += 1
        self.update_scoreboard()

    def r_paddle_score(self):
        self.r_score += 1
        self.update_scoreboard()

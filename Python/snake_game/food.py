from turtle import Turtle
import random

STRETCH = 0.5
MAX_COORDS = 280


class Food(Turtle):
    def __init__(self):
        super().__init__()
        self.shape("circle")
        self.penup()
        self.shapesize(stretch_wid=STRETCH, stretch_len=STRETCH)
        self.color("blue")
        self.speed("fastest")
        self.refresh()

    def refresh(self):
        random_x = random.randint(-MAX_COORDS, MAX_COORDS)
        random_y = random.randint(-MAX_COORDS, MAX_COORDS)
        self.goto(x=random_x, y=random_y)

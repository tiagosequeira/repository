from turtle import Turtle

ALIGN = "center"
FONT = ("Courier", 24, "normal")


class Scoreboard(Turtle):
    def __init__(self):
        super().__init__()
        self.score = 0
        with open("high_score.txt") as file:
            self.high_score = int(file.read())
        self.penup()
        self.color("white")
        self.goto(0, 270)
        self.hideturtle()
        self.write_score()

    def write_score(self):
        self.clear()
        self.write(arg=f"SCORE = {self.score} HIGH SCORE = {self.high_score}", align=ALIGN, font=FONT)

    def increase_score(self):
        self.score += 1
        self.write_score()

    # def game_over(self):
    #     self.home()
    #     self.write(arg="GAME OVER", align=ALIGN, font=FONT)

    def reset(self):
        if self.score > self.high_score:
            self.high_score = self.score
            with open("high_score.txt", mode="w") as file:
                file.write(f"{self.high_score}")
        self.score = 0
        self.write_score()

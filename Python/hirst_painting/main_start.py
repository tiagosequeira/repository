from turtle import Turtle, Screen
import random

turtle = Turtle()
turtle.shape("turtle")


# for _ in range(15):
#     turtle.forward(10)
#     turtle.penup()
#     turtle.forward(10)
#     turtle.pendown()

def change_color():
    R = random.random()
    B = random.random()
    G = random.random()

    turtle.pencolor(R, G, B)


def random_direction():
    n = random.randint(0, 4)
    turtle.setheading(n * 90)


# for n in range(3, 11):
#     change_color()
#     for _ in range(n):
#         turtle.forward(100)
#         turtle.right(360 / n)

turtle.pensize(1)
turtle.speed("fastest")
turtle.hideturtle()


# while True:
#     change_color()
#     turtle.forward(30)
#     random_direction()


def draw_spiral(n):
    for _ in range(int(360 / n)):
        change_color()
        turtle.circle(100)
        turtle.setheading(turtle.heading() + n)


draw_spiral(10)

screen = Screen()
screen.exitonclick()

package org.academiadecodigo.nanderthals;

import org.academiadecodigo.nanderthals.Commands.Command;
import org.academiadecodigo.nanderthals.Commands.CommandHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    private final LinkedList<Room> roomLinkedList;
    private final ExecutorService fixedPool = Executors.newCachedThreadPool();

    public Server() {
        this.roomLinkedList = new LinkedList<>();
    }

    public void start() throws IOException {
        System.out.print("Port ? ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int portNumber = Integer.parseInt(reader.readLine());

        ServerSocket serverSocket = new ServerSocket(portNumber);
        System.out.println("# Chat server started on port " + portNumber + " #");

        Room room = new Room("MainRoom");
        roomLinkedList.add(room);

        while (true) {
            Socket clientSocket = serverSocket.accept();
            System.out.println("# NEW CLIENT CONNECTED #");

            ServerWorker serverWorker = new ServerWorker(clientSocket, this, room);
            room.addClient(serverWorker);

            fixedPool.submit(serverWorker);
        }
    }

    public void sendToAll(String message, ServerWorker client) {
        synchronized (roomLinkedList) {
            for (Room room : roomLinkedList) {
                synchronized (room.getServerWorkersLinkedList()) {
                    for (ServerWorker serverWorker : room.getServerWorkersLinkedList()) {
                        if (serverWorker == client) continue;
                        serverWorker.sendMessage(message);
                    }
                }
            }
        }
    }

    public void addRoom(Room newRoom) {
        synchronized (roomLinkedList) {
            roomLinkedList.add(newRoom);
        }
    }

    public LinkedList<Room> getRoomLinkedList() {
        return roomLinkedList;
    }


    public class ServerWorker implements Runnable {
        private Socket clientSocket;
        private BufferedReader reader;
        private PrintWriter writer;
        private String username;
        private Server server;
        private Room currentRoom;


        public ServerWorker(Socket clientSocket, Server server, Room room) {
            this.clientSocket = clientSocket;
            this.server = server;
            this.currentRoom = room;
            streams();
        }

        private void streams() {
            try {
                reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                writer = new PrintWriter(clientSocket.getOutputStream(), true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void run() {
            handleClient();
        }

        public void handleClient() {
            try {
                String clientMessage;

                writer.println("Enter your username:");
                username = reader.readLine();

                currentRoom.sendToRoom(username + " has joined the chat.", this);

                while ((clientMessage = reader.readLine()) != null) {
                    if (clientMessage.startsWith("/")) {
                        Command command = CommandHandler.handleCommand(clientMessage.split(" ")[0]);
                        command.handle(clientMessage, server, this);
                        continue;
                    }

                    currentRoom.sendToRoom("<" + username + "> " + clientMessage, this);
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                currentRoom.removeClient(this);
                sendToAll(username + " has left the chat.", this);
                try {
                    reader.close();
                    writer.close();
                    clientSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public PrintWriter getWriter() {
            return writer;
        }

        public void sendMessage(String message) {
            writer.println(message);
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsername() {
            return username;
        }

        public LinkedList<Room> getRooms() {
            return server.getRoomLinkedList();
        }

        public Room getCurrentRoom() {
            return currentRoom;
        }

        public void setCurrentRoom(Room currentRoom) {
            this.currentRoom = currentRoom;
        }

    }
}

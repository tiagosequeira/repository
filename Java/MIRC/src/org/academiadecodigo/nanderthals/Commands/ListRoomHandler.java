package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Room;
import org.academiadecodigo.nanderthals.Server;

import java.util.List;

public class ListRoomHandler implements Command {
    @Override
    public void handle(String message, Server server, Server.ServerWorker serverWorker) {

        List<Room> roomList = server.getRoomLinkedList();
        StringBuilder sb = new StringBuilder("\n");

        for (Room room : roomList) {
            sb.append(room.getRoomName());
            sb.append("\n");
        }

        serverWorker.sendMessage(sb.toString());

    }
}

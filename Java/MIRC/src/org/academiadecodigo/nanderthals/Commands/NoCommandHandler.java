package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Server;

import java.io.PrintWriter;

public class NoCommandHandler implements Command {
    @Override
    public void handle(String message, Server server, Server.ServerWorker serverWorker) {
        PrintWriter writer = serverWorker.getWriter();

        writer.println("The command - " + message.split(" ")[0] + ", doesn't exist ");
        writer.println("Try writing /help to get a full list of available commands.");
    }
}

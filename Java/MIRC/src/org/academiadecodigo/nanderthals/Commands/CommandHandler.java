package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Messages;

public enum CommandHandler {
    WHISPER("/whisper", new WhisperCommandHandler(), Messages.WHISPER),
    NO_COMMAND_HANDLER("", new NoCommandHandler(), ""),
    LIST("/list", new ListUsersCommandHandler(), ""),
    HELP("/help", new HelpCommandHandler(), ""),
    CHANGE_USERNAME("/change", new ChangeUsernameHandler(), ""),
    CREATE_ROOM("/createroom", new CreateRoomCommandHandler(), ""),
    CHANGE_ROOM("/changeroom", new ChangeRoomHandler(), ""),
    LIST_ROOM("/listrooms", new ListRoomHandler(), ""),
    BROADCAST("/broadcast", new BroadcastCommandHandler(), Messages.MESSAGE);


    private String command;
    private Command commandHandler;
    private String commandUsage;

    CommandHandler(String command, Command commandHandler, String commandUsage) {
        this.command = command;
        this.commandHandler = commandHandler;
        this.commandUsage = "< " + command + " > " + commandUsage;
    }

    public static Command handleCommand(String command) {
        for (CommandHandler value : values()) {
            if (value.command.equals(command)) {
                return value.commandHandler;
            }
        }
        return NO_COMMAND_HANDLER.commandHandler;
    }

    public static String getAllAvailableCommands() {
        StringBuilder stringBuilder = new StringBuilder();
        for (CommandHandler value : values()) {
            if (!value.command.isEmpty()) {
                stringBuilder.append(value.command);
                stringBuilder.append("\n");
            }
        }

        return stringBuilder.toString();
    }

}

package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Server;

import java.io.PrintWriter;

public class HelpCommandHandler implements Command {
    @Override
    public void handle(String message, Server server, Server.ServerWorker serverWorker) {
        PrintWriter writer = serverWorker.getWriter();
        writer.println(CommandHandler.getAllAvailableCommands());
    }
}

package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Messages;
import org.academiadecodigo.nanderthals.Room;
import org.academiadecodigo.nanderthals.Server;

public class ChangeRoomHandler implements Command {
    @Override
    public void handle(String message, Server server, Server.ServerWorker serverWorker) {
        String[] splitMessage = message.split(" ");

        String roomName = splitMessage[1];

        Room targetRoom = null;
        for (Room room : server.getRoomLinkedList()) {
            if (room.getRoomName().equalsIgnoreCase(roomName)) {
                targetRoom = room;
                break;
            }
        }

        if (targetRoom == null) {
            serverWorker.sendMessage(Messages.ROOM_NOT_FOUND + roomName);
            return;
        }

        Room currentRoom = serverWorker.getCurrentRoom();
        if (currentRoom != null) {
            currentRoom.removeClient(serverWorker);
        }

        targetRoom.addClient(serverWorker);
        serverWorker.setCurrentRoom(targetRoom);

        serverWorker.sendMessage(Messages.SWITCHED_TO_ROOM + targetRoom.getRoomName());

    }
}

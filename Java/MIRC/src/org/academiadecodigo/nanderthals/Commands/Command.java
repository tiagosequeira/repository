package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Server;

public interface Command {
    void handle(String message, Server server, Server.ServerWorker serverWorker);
}

package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Messages;
import org.academiadecodigo.nanderthals.Room;
import org.academiadecodigo.nanderthals.Server;

import java.io.PrintWriter;

public class CreateRoomCommandHandler implements Command {
    @Override
    public void handle(String message, Server server, Server.ServerWorker serverWorker) {
        PrintWriter writer = serverWorker.getWriter();
        String[] parts = message.split("\\s+", 2);

        if (parts.length == 2) {
            String roomName = parts[1];
            Room newRoom = new Room(roomName);
            server.addRoom(newRoom);

            writer.println(Messages.ROOM + roomName + Messages.ROOM_CREATED);
            return;
        }
        writer.println(Messages.INVALID_COMMAND + " " + "Usage: /createroom <room_name>");


    }
}

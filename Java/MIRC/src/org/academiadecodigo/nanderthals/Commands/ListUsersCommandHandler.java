package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Room;
import org.academiadecodigo.nanderthals.Server;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class ListUsersCommandHandler implements Command {
    @Override
    public void handle(String message, Server server, Server.ServerWorker serverWorker) {
        List<Server.ServerWorker> clients = new LinkedList<>();

        for (Room room : server.getRoomLinkedList()) {
            clients.addAll(room.getServerWorkersLinkedList());
        }

        PrintWriter writer = serverWorker.getWriter();

        for (Server.ServerWorker client : clients) {
            writer.println(client.getUsername() + " is in the room: " + client.getCurrentRoom().getRoomName());
        }
    }
}

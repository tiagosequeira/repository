package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Messages;
import org.academiadecodigo.nanderthals.Server;
import org.academiadecodigo.nanderthals.Server.ServerWorker;

import java.io.PrintWriter;
import java.util.List;

public class ChangeUsernameHandler implements Command {

    @Override
    public void handle(String message, Server server, ServerWorker serverWorker) {
        List<ServerWorker> clients = serverWorker.getCurrentRoom().getServerWorkersLinkedList();
        PrintWriter writer = serverWorker.getWriter();

        String[] parts = message.split("\\s+", 2);

        ServerWorker oldUser = null;

        synchronized (clients) {
            for (ServerWorker worker : clients) {
                if (worker == serverWorker) {
                    oldUser = worker;
                    break;
                }
            }

            if (oldUser != null) {
                String previousUsername = oldUser.getUsername();
                oldUser.setUsername(parts[1]);

                writer.println(Messages.MY_USERNAME_CHANGED + parts[1]);
                server.sendToAll(previousUsername + Messages.THEIR_USERNAME_CHANGED + parts[1], oldUser);
                return;
            }
            writer.println(Messages.USERNAME + parts[1] + Messages.DOESNT_EXIST);
        }
    }
}

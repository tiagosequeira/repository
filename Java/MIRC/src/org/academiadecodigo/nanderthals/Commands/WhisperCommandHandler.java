package org.academiadecodigo.nanderthals.Commands;

import org.academiadecodigo.nanderthals.Room;
import org.academiadecodigo.nanderthals.Server;

import java.util.LinkedList;
import java.util.List;

public class WhisperCommandHandler implements Command {

    @Override
    public void handle(String message, Server server, Server.ServerWorker serverWorker) {
        List<Server.ServerWorker> clients = new LinkedList<>();

        for (Room room : server.getRoomLinkedList()) {
            clients.addAll(room.getServerWorkersLinkedList());
        }

        String[] parts = message.split("\\s+", 3);

        synchronized (clients) {
            for (Server.ServerWorker worker : clients) {
                if (worker.getUsername().equals(parts[1])) {
                    worker.sendMessage(serverWorker.getUsername() + " (whisper) -> " + parts[2]);
                    return;
                }
            }
        }
    }
}

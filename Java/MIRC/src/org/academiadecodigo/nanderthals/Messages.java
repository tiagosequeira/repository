package org.academiadecodigo.nanderthals;

public class Messages {

    public static final String WHISPER = "<name> <message>";
    public static final String MESSAGE = "<message>";
    public static final String ROOM_NAME = "<room_name>";
    public static final String ROOM = "Room";
    public static final String ROOM_NOT_FOUND = "Room not found: ";
    public static final String ROOM_CREATED = " has been created";
    public static final String SWITCHED_TO_ROOM = "Switched to room: ";
    public static final String USERNAME = "Username: ";
    public static final String MY_USERNAME_CHANGED = "Your username has been changed to: ";
    public static final String THEIR_USERNAME_CHANGED = " has changed their username to ";
    public static final String DOESNT_EXIST = " does not exist";
    public static final String INVALID_COMMAND = "Invalid command ";
}

package org.academiadecodigo.nanderthals;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Room {

    private final String roomName;
    private final List<Server.ServerWorker> serverWorkersLinkedList;

    public Room(String roomName) {
        this.roomName = roomName;
        this.serverWorkersLinkedList = Collections.synchronizedList(new LinkedList<>());
    }

    public String getRoomName() {
        return roomName;
    }

    public List<Server.ServerWorker> getServerWorkersLinkedList() {
        return serverWorkersLinkedList;
    }

    public void addClient(Server.ServerWorker serverWorker) {
        synchronized (serverWorkersLinkedList) {
            serverWorkersLinkedList.add(serverWorker);
        }
    }

    public void removeClient(Server.ServerWorker serverWorker) {
        synchronized (serverWorkersLinkedList) {
            serverWorkersLinkedList.remove(serverWorker);
        }
    }

    public void sendToRoom(String message, Server.ServerWorker client) {
        synchronized (serverWorkersLinkedList) {
            for (Server.ServerWorker serverWorker : serverWorkersLinkedList) {
                if (serverWorker == client) continue;
                serverWorker.sendMessage(message);
            }
        }
    }
}


package org.academiadecodigo.nanderthals;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        System.out.print("Hostname ? ");
        Scanner scanner1 = new Scanner(System.in);
        String hostname = scanner1.nextLine();

        System.out.print("Port ? ");
        Scanner scanner2 = new Scanner(System.in);
        int port = Integer.parseInt(scanner2.nextLine());

        while (true) {
            System.out.print("String ? ");
            Scanner scanner3 = new Scanner(System.in);
            String string = scanner3.nextLine();

            Socket clientSocket = new Socket(hostname, port);

            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);

            out.print(string);

            out.close();
            clientSocket.close();
        }
    }
}